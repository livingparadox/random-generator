class RandDateTime
  def self.rand(options = {})
    min = options[:min].nil? ? 0 : parse(options[:min])
    max = options[:max].nil? ? DateTime.now.to_i : parse(options[:max])
    count = options[:count].nil? ? 1 : options[:count].to_i
    count = max-min if count > max-min
    sample(min..max, count)
  end

  def self.parse(time)
    DateTime.parse(time).to_i
  end

  def self.sample(range, count)
    result = Set.new
    while result.length < count
      result << DateTime.strptime(Random.rand(range).to_s, "%s").to_s(:db)
    end
    result
  end
end
